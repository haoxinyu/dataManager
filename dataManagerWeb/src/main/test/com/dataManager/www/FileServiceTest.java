package com.dataManager.www;

import com.dataManager.www.entity.File;
import com.dataManager.www.entity.FileCatalog;
import com.dataManager.www.service.FileCatalogService;
import com.dataManager.www.service.FileService;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.sql.SQLException;

import static org.junit.Assert.assertNotNull;

public class FileServiceTest extends BaseTest{
    @Autowired
    private FileService fileService;
    @Before
    public void setup(){
        assertNotNull(fileService);
    }

    @Test
    public void testAdd() throws SQLException, IOException {
        File file = new File();
        file.setFileName("测试");
        file.setFilePath("c://");
        file.setFileSize(1000);
        file.setFileType("txt");
        file.setFileVersion(1);
        file.setLabels("测试数据,无意义");
        file.setUserId(1L);
        fileService.add(file);
    }


}
