package com.dataManager.www;

import com.dataManager.www.entity.User;
import com.dataManager.www.service.UserService;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

import static org.junit.Assert.assertNotNull;

public class UserServiceTest extends BaseTest{
    @Autowired
    private UserService userService;
    @Before
    public void setup(){
        assertNotNull(userService);
    }

    @Test
    public void testAdd() throws SQLException, IOException {
        User user = new User();
        user.setUsername("aaa");
        user.setPassword("123");
        user.setPhone("33333333333");
        user.setEmail("aaa3@qq.cpm");
        userService.add(user);
    }

    @Test
    public void testGet(){
        HashMap userMap = new HashMap();
        userMap.put("username","haoxinyu");
        userMap.put("password","123");
        User user = userService.get(userMap);
        System.out.println(user.getId());
    }

    @Test
    public void testUpdate(){
        User user = new User();
        user.setUsername("haoxinyu");
        user.setPassword("123");
        List<User> userList = userService.listByObject(user);
        User user1 =userList.get(0);
        System.out.println(user1.getId());
        user1.setDiskSize(1000);
        userService.update(user1);

    }
    @Test
    public void testUpdateMine(){
        User user = new User();
        user.setUsername("haoxinyu");
        user.setPassword("123");
        List<User> userList = userService.listByObject(user);
        User user1 =userList.get(0);
        System.out.println(user1.getId());
        user1.setMineSize(1000);
        userService.update(user1);

    }

    @Test
    public void testGet1(){
        User user = new User();
        user.setUsername("haoxinyu");
        user.setPassword("123");
        List<User> userList = userService.listByObject(user);
        User user1 =userList.get(0);
        System.out.println(user1.getUsername());
    }

}
