package com.dataManager.www;

import com.dataManager.www.entity.Catalogue;
import com.dataManager.www.service.CatalogueService;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.sql.SQLException;

import static org.junit.Assert.assertNotNull;

public class CatalogueServiceTest extends BaseTest{
    @Autowired
    private CatalogueService catalogueService;
    @Before
    public void setup(){
        assertNotNull(catalogueService);
    }

    @Test
    public void testAdd() throws SQLException, IOException {
        Catalogue catalogue = new Catalogue();
        catalogue.setCatalogName("测试一级目录");
        catalogue.setParentId(-1L);
        catalogue.setUserId(1L);
        catalogueService.add(catalogue);
    }


}
