package com.dataManager.www;

import com.dataManager.www.entity.FileCatalog;
import com.dataManager.www.entity.User;
import com.dataManager.www.service.FileCatalogService;
import com.dataManager.www.service.UserService;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;

import static org.junit.Assert.assertNotNull;

public class FileCatalogServiceTest extends BaseTest{
    @Autowired
    private FileCatalogService fileCatalogService;
    @Before
    public void setup(){
        assertNotNull(fileCatalogService);
    }

    @Test
    public void testAdd() throws SQLException, IOException {
        FileCatalog fileCatalog = new FileCatalog();
        fileCatalog.setCatalogId(1L);
        fileCatalog.setFileId(1L);
        fileCatalogService.add(fileCatalog);
    }


}
