package com.dataManager.www;

import com.dataManager.www.entity.FileCatalog;
import com.dataManager.www.entity.Share;
import com.dataManager.www.service.FileCatalogService;
import com.dataManager.www.service.ShareService;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Date;

import static org.junit.Assert.assertNotNull;

public class ShareServiceTest extends BaseTest {
    @Autowired
    private ShareService shareService;

    @Before
    public void setup() {
        assertNotNull(shareService);
    }

    @Test
    public void testAdd() throws SQLException, IOException {
        Share share = new Share();
        share.setFileId(1L);
        share.setStartTime(new Date());
        share.setEndTime(new Date());
        share.setToUserId(1L);
        share.setFileId(1L);
        shareService.add(share);
    }
}