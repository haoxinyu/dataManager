package com.dataManager.www.utils;

public interface SystemCode {
	

	public static final String SUCCESS = "success";

	public static final String ERROR = "error";

	public static final String BUSY = "busy";

	public static final String PASSWORD_ERROR = "passwordError";

	public static final String CAPTCHA_ERROR = "captchaError";

	public static final String ADMIN_USER = "administrators";

	public static final String NORMAL_USER = "general";

	public static final String FULL_DISK_SIZE = "freeDiskSizeEmpty";

	
	/**
	 * 验证失败
	 */
	public static final String SYS_001 = "50001";
	
	/**
	 * 不合法的凭证类型
	 */
	public static final String SYS_002 = "50002";
	
	/**
	 * 不合法的媒体文件类型
	 */
	public static final String SYS_003 = "50003";
	
	/**
	 * 不合法的文件类型
	 */
	public static final String SYS_004 = "50004";
	
	/**
	 * 图片上传失败
	 */
	public static final String SYS_005 = "50005";
	
	/**
	 * 无当前操作权限
	 */
	public static final String SYS_006 = "50006";
	
	/**
	 * 操作对象不存在
	 */
	public static final String SYS_007 = "50007";
	
	/**
	 * 请求方式错误
	 */
	public static final String SYS_008 = "50008";
	
	/**
	 * 加密失败
	 */
	public static final String SYS_009 = "50009";
	
	/**
	 * 解密失败
	 */
	public static final String SYS_010 = "50010";
	
	/**
	 * 登陆超时
	 */
	public static final String SYS_011 = "50011";
}
