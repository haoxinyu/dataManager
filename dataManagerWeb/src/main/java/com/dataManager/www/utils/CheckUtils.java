package com.dataManager.www.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CheckUtils {

    public static boolean checkEmail(String target){
        String check = "^([a-z0-9A-Z]+[-|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$";
        Pattern regex = Pattern.compile(check);
        Matcher matcher = regex.matcher(target);
        boolean isMatched = matcher.matches();
        System.out.println(isMatched);
        return isMatched;
    }

    public static boolean cheakPhone(String target){
        String check = "^(13[4,5,6,7,8,9]|15[0,8,9,1,7]|188|187)\\d{8}$";
        Pattern regex = Pattern.compile(check);
        Matcher matcher = regex.matcher(target);
        boolean isMatched = matcher.matches();
        System.out.println(isMatched);
        return isMatched;
    }

}
