package com.dataManager.www.dao;

import com.dataManager.www.entity.Share;
import com.dev.base.mybatis.dao.BaseMybatisDao;

public interface ShareDao extends BaseMybatisDao<Share,Long> {
}
