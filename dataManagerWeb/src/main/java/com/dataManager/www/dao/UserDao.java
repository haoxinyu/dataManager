package com.dataManager.www.dao;

import com.dataManager.www.entity.User;
import com.dev.base.mybatis.dao.BaseMybatisDao;

import java.util.List;

public interface UserDao extends BaseMybatisDao<User, Long> {
    List<User> listAll();

    double sumTotalDiskSizeByUser(User user);
}
