package com.dataManager.www.dao;

import com.dataManager.www.entity.File;
import com.dev.base.mybatis.dao.BaseMybatisDao;

public interface FileDao extends BaseMybatisDao<File, Long> {
    double calculateFileSizeByUserId(Long userId);
}
