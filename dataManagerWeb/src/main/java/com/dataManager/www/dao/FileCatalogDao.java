package com.dataManager.www.dao;

import com.dataManager.www.entity.FileCatalog;
import com.dev.base.mybatis.dao.BaseMybatisDao;

public interface FileCatalogDao extends BaseMybatisDao<FileCatalog,Long> {
}
