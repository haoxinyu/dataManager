package com.dataManager.www.dao;

import com.dataManager.www.entity.Catalogue;
import com.dev.base.mybatis.dao.BaseMybatisDao;

public interface CatalogueDao extends BaseMybatisDao<Catalogue,Long> {
}
