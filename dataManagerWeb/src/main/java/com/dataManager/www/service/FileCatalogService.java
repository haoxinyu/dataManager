package com.dataManager.www.service;

import com.dataManager.www.entity.Catalogue;
import com.dataManager.www.entity.FileCatalog;
import com.dev.base.mybatis.service.BaseMybatisService;

public interface FileCatalogService  extends BaseMybatisService<FileCatalog, Long> {
}
