package com.dataManager.www.service;

import com.dataManager.www.entity.Catalogue;
import com.dataManager.www.entity.User;
import com.dev.base.mybatis.service.BaseMybatisService;

public interface CatalogueService extends BaseMybatisService<Catalogue, Long> {
}
