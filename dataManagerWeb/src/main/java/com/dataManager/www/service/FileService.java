package com.dataManager.www.service;

import com.dataManager.www.entity.File;
import com.dataManager.www.entity.User;
import com.dev.base.mybatis.service.BaseMybatisService;

public interface FileService extends BaseMybatisService<File, Long> {
    double calculateFileSizeByUserId(Long userId);
}
