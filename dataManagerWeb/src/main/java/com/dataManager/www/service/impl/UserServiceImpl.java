package com.dataManager.www.service.impl;

import com.dataManager.www.dao.UserDao;
import com.dataManager.www.entity.User;
import com.dataManager.www.service.UserService;
import com.dev.base.mybatis.service.impl.BaseMybatisServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl extends BaseMybatisServiceImpl<User, Long, UserDao> implements UserService {
    @Override
    public List<User> listAll() {
        return getMybatisDao().listAll();
    }

    @Override
    public double sumTotalDiskSizeByUser(User user) {
        return getMybatisDao().sumTotalDiskSizeByUser(user);
    }
}
