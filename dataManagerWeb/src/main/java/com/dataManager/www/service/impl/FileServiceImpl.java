package com.dataManager.www.service.impl;

import com.dataManager.www.dao.FileDao;
import com.dataManager.www.entity.File;
import com.dataManager.www.service.FileService;
import com.dev.base.mybatis.service.impl.BaseMybatisServiceImpl;
import org.springframework.stereotype.Service;

@Service
public class FileServiceImpl extends BaseMybatisServiceImpl<File, Long, FileDao> implements FileService {
    public double calculateFileSizeByUserId(Long userId){
        return getMybatisDao().calculateFileSizeByUserId(userId);
    }
}
