package com.dataManager.www.service.impl;

import com.dataManager.www.dao.ShareDao;
import com.dataManager.www.entity.Share;
import com.dataManager.www.service.ShareService;
import com.dev.base.mybatis.service.impl.BaseMybatisServiceImpl;
import org.springframework.stereotype.Service;

@Service
public class ShareServiceImpl  extends BaseMybatisServiceImpl<Share, Long, ShareDao> implements ShareService {
}
