package com.dataManager.www.service;

import com.dataManager.www.entity.Share;
import com.dataManager.www.entity.User;
import com.dev.base.mybatis.service.BaseMybatisService;

public interface ShareService extends BaseMybatisService<Share, Long> {
}
