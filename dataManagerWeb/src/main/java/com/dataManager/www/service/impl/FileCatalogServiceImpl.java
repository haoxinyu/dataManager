package com.dataManager.www.service.impl;

import com.dataManager.www.dao.FileCatalogDao;
import com.dataManager.www.entity.FileCatalog;
import com.dataManager.www.service.FileCatalogService;
import com.dev.base.mybatis.service.impl.BaseMybatisServiceImpl;
import org.springframework.stereotype.Service;

@Service
public class FileCatalogServiceImpl extends BaseMybatisServiceImpl<FileCatalog, Long, FileCatalogDao> implements FileCatalogService {
}
