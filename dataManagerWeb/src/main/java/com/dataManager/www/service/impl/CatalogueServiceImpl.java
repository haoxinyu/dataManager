package com.dataManager.www.service.impl;

import com.dataManager.www.dao.CatalogueDao;
import com.dataManager.www.entity.Catalogue;
import com.dataManager.www.service.CatalogueService;
import com.dev.base.mybatis.service.impl.BaseMybatisServiceImpl;
import org.springframework.stereotype.Service;

@Service
public class CatalogueServiceImpl extends BaseMybatisServiceImpl<Catalogue, Long, CatalogueDao> implements CatalogueService {
}
