package com.dataManager.www.service;

import com.dataManager.www.entity.User;
import com.dev.base.mybatis.service.BaseMybatisService;

import java.util.List;

public interface UserService extends BaseMybatisService<User, Long> {
    List<User> listAll();
    double sumTotalDiskSizeByUser(User user);
}
