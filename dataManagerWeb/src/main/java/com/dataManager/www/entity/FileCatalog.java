package com.dataManager.www.entity;

import com.dev.base.mybatis.BaseMybatisEntity;

/**
 * 文件存放在目录位置的实体类
 */
public class FileCatalog extends BaseMybatisEntity {

    private static final long serialVersionUID = 1L;

    private Long fileId;
    private Long catalogId;

    public Long getFileId() {
        return fileId;
    }

    public void setFileId(Long fileId) {
        this.fileId = fileId;
    }

    public Long getCatalogId() {
        return catalogId;
    }

    public void setCatalogId(Long catalogId) {
        this.catalogId = catalogId;
    }
}
