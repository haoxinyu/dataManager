package com.dataManager.www.entity;

import com.dev.base.mybatis.BaseMybatisEntity;

/**
 * 用户实体类
 */
public class User extends BaseMybatisEntity {

    private static final long serialVersionUID = 1L;
    private String username;
    private String password;
    private String email;
    private String phone;
    private String role;
    private String status;
    private double diskSize;
    private double applySize;
    private double mineSize;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public double getDiskSize() {
        return diskSize;
    }

    public void setDiskSize(double diskSize) {
        this.diskSize = diskSize;
    }

    public double getApplySize() {
        return applySize;
    }

    public void setApplySize(double applySize) {
        this.applySize = applySize;
    }

    public double getMineSize() {
        return mineSize;
    }

    public void setMineSize(double mineSize) {
        this.mineSize = mineSize;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", email='" + email + '\'' +
                ", phone='" + phone + '\'' +
                ", role='" + role + '\'' +
                ", status='" + status + '\'' +
                ", diskSize=" + diskSize +
                ", applySize=" + applySize +
                ", mineSize=" + mineSize +
                '}';
    }
}
