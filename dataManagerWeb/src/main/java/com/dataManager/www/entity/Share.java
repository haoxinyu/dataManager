package com.dataManager.www.entity;

import com.dev.base.mybatis.BaseMybatisEntity;

import java.util.Date;

/**
 * 分享数据的实体类
 */
public class Share extends BaseMybatisEntity {
    private static final long serialVersionUID = 1L;

    private Long userId;
    private Long fileId;
    private Long toUserId;
    private Date startTime;
    private Date endTime;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getFileId() {
        return fileId;
    }

    public void setFileId(Long fileId) {
        this.fileId = fileId;
    }

    public Long getToUserId() {
        return toUserId;
    }

    public void setToUserId(Long toUserId) {
        this.toUserId = toUserId;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }
}
