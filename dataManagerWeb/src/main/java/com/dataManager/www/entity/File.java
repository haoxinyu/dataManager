package com.dataManager.www.entity;

import com.dev.base.mybatis.BaseMybatisEntity;

/**
 * 文件实体类
 */
public class File extends BaseMybatisEntity {

    private static final long serialVersionUID = 1L;

    private String fileName;
    private double fileSize;
    private String fileType;
    private String labels;
    private int fileVersion;
    private String filePath;
    private int watchCount;
    private int downloadCount;
    private Long userId;

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public double getFileSize() {
        return fileSize;
    }

    public void setFileSize(double fileSize) {
        this.fileSize = fileSize;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public String getLabels() {
        return labels;
    }

    public void setLabels(String labels) {
        this.labels = labels;
    }

    public int getFileVersion() {
        return fileVersion;
    }

    public void setFileVersion(int fileVersion) {
        this.fileVersion = fileVersion;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public int getWatchCount() {
        return watchCount;
    }

    public void setWatchCount(int watchCount) {
        this.watchCount = watchCount;
    }

    public int getDownloadCount() {
        return downloadCount;
    }

    public void setDownloadCount(int downloadCount) {
        this.downloadCount = downloadCount;

    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "File{" +
                "id=" + id +
                ", fileName='" + fileName + '\'' +
                ", fileSize=" + fileSize +
                ", fileType='" + fileType + '\'' +
                ", labels='" + labels + '\'' +
                ", fileVersion=" + fileVersion +
                ", filePath='" + filePath + '\'' +
                ", watchCount=" + watchCount +
                ", downloadCount=" + downloadCount +
                ", userId=" + userId +
                '}';
    }
}
