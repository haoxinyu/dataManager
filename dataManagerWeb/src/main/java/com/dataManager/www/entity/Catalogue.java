package com.dataManager.www.entity;

import com.dev.base.mybatis.BaseMybatisEntity;

/**
 * 目录实体类
 */
public class Catalogue extends BaseMybatisEntity {

    private static final long serialVersionUID = 1L;

    private String catalogName;
    private Long parentId;
    private Long UserId;

    public String getCatalogName() {
        return catalogName;
    }

    public void setCatalogName(String catalogName) {
        this.catalogName = catalogName;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public Long getUserId() {
        return UserId;
    }

    public void setUserId(Long userId) {
        UserId = userId;
    }
}
