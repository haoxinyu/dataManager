package com.dataManager.www.controller;

import com.dataManager.www.entity.User;
import com.dataManager.www.service.UserService;
import com.dataManager.www.utils.JsonUtils;
import com.dataManager.www.utils.SystemCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import java.util.HashMap;

@RestController
@RequestMapping(value = "/sys/usertest")
public class TestUserController {

    @Autowired
    private UserService userService;

    @RequestMapping(value = "get/{username}",method = RequestMethod.GET)
    public User getUser(@PathVariable String username){
        HashMap userMap = new HashMap();
        userMap.put("username",username);
        User loginUser =userService.get(userMap);
        return loginUser;
    }

    @RequestMapping(value = "get",method = RequestMethod.POST)
    public User getUserPost(@RequestBody User user){
        HashMap userMap = new HashMap();
        userMap.put("username",user.getUsername());
        User loginUser =userService.get(userMap);
        return loginUser;
    }

    @RequestMapping(value = "get2",method = RequestMethod.POST)
    public User getUserPost2(@RequestParam(value = "username",required = true) String username){
        HashMap userMap = new HashMap();
        userMap.put("username",username);
        User loginUser =userService.get(userMap);
        return loginUser;
    }



}
