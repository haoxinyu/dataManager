<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ include file="/static/taglib.jsp"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>layui</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="lib/layui-v2.6.3/css/layui.css" media="all">
    <link rel="stylesheet" href="css/public.css" media="all">
</head>
<body>
<div class="layuimini-container">
    <div class="layuimini-main">

        <fieldset class="table-search-fieldset">
            <legend>搜索信息</legend>
            <div style="margin: 10px 10px 10px 10px">
                <form class="layui-form layui-form-pane" action="">
                    <div class="layui-form-item">
                        <div class="layui-inline">
                            <label class="layui-form-label">文件名</label>
                            <div class="layui-input-inline">
                                <input type="text" name="fileName" id="fileName" autocomplete="off" class="layui-input">
                            </div>
                        </div>
                        <div class="layui-inline">
                            <label class="layui-form-label">文件类型</label>
                            <div class="layui-input-inline">
                                <input type="text" name="fileType" id="fileType" autocomplete="off" class="layui-input">
                            </div>
                        </div>
                        <div class="layui-inline">
                            <label class="layui-form-label">文件标签</label>
                            <div class="layui-input-inline">
                                <input type="text" name="labels" id="labels" autocomplete="off" class="layui-input">
                            </div>
                        </div>
                        <div class="layui-inline">
                            <button type="submit" class="layui-btn layui-btn-primary"  lay-submit lay-filter="data-search-btn"><i class="layui-icon"></i> 搜 索</button>
                        </div>
                    </div>
                </form>
            </div>
        </fieldset>

        <table class="layui-hide" id="currentTableId" lay-filter="currentTableFilter"></table>
        <script type="text/html" id="currentTableBar">
            <a class="layui-btn layui-btn-normal layui-btn-xs data-count-edit" lay-event="downLoad">下载</a>
        </script>
    </div>
</div>
<script src="lib/layui-v2.6.3/layui.js" charset="utf-8"></script>
<script>
    layui.use(['form', 'table'], function () {
        var $ = layui.jquery,
            form = layui.form,
            table = layui.table;

        table.render({
            elem: '#currentTableId',
            url: '/sys/file/listFile',
            cols: [[
                {field:'id', title: 'ID', hide:true},
                {field: 'fileName', width: 80, title: '文件名'},
                {field: 'fileSize', width: 100, title: '文件大小', sort: true},
                {field: 'fileType', width: 120, title: '文件类型', sort: true},
                {field: 'labels', title: '文件标签',width: 120},
                {field: 'fileVersion', width: 120, title: '文件版本', sort: true},
                {field: 'filePath', width: 160, title: '文件路径'},
                {field: 'watchCount', width: 100, title: '浏览量', sort: true},
                {field: 'downloadCount', width: 100, title: '下载量', sort: true},
                {field: 'userId', width: 80, title: '用户id', sort: true},
                {title: '操作', minWidth: 150, toolbar: '#currentTableBar', align: "center"}
            ]],
            limits: [10, 15, 20, 25, 50, 100],
            limit: 15,
            page: true,
            skin: 'line'
        });

        // 监听搜索操作
        form.on('submit(data-search-btn)', function (data) {
            console.info(data.data)
            table.reload('currentTableId', {
                url: '/sys/file/listFile',
                where: {
                    fileName : $("#fileName").val(),
                    fileType : $("#fileType").val(),
                    labels : $("#labels").val(),
                }
            });

            return false;
        });

        table.on('tool(currentTableFilter)', function (obj) {
            var data = obj.data;
            console.info(data)
            if (obj.event === 'downLoad') {
                var params = {// 参数
                    id:data.id
                };
                var form = document.createElement('form')
                form.id = 'form'
                form.name = 'form'
                form.enctype = 'application/x-www-form-urlencoded'
                document.body.appendChild (form)
                for (var obj in params) {
                    if (params.hasOwnProperty(obj)) {
                        var input = document.createElement('input')
                        input.type='hidden'
                        input.name = obj;
                        input.value = params[obj]
                        form.appendChild(input)
                    }
                }
                //请求方式POST提交时 默认Content-Type就是application/x-www-form-urlencoded
                form.method = "POST"
                form.action = "/sys/file/download"
                form.submit();
                document.body.removeChild(form)
            }

        });
    });
</script>

</body>
</html>