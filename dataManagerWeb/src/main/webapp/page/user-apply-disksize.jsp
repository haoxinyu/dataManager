<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ include file="/static/taglib.jsp"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>申请容量</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="lib/layui-v2.6.3/css/layui.css" media="all">
    <link rel="stylesheet" href="css/public.css" media="all">
    <style>
        .layui-form-item .layui-input-company {width: auto;padding-right: 10px;line-height: 38px;}
    </style>
</head>
<body>
<div class="layuimini-container">
    <div class="layuimini-main">

        <div class="layui-form layuimini-form">

            <div class="layui-form-item">
                <label class="layui-form-label required">申请容量（单位：MB）</label>
                <div class="layui-input-block">
                    <input type="number" name="applySize" id="applySize" lay-verify="required" lay-reqtext="申请的容量不能为空" placeholder="请输入申请的容量"  value="" class="layui-input" readonly="">
                    <br/>
                    <div id="slideTest1"></div>
                </div>
            </div>

            <div class="layui-form-item">
                <div class="layui-input-block">
                    <button class="layui-btn layui-btn-normal" lay-submit lay-filter="saveBtn">确认申请</button>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="lib/jquery-3.4.1/jquery-3.4.1.min.js" charset="utf-8"></script>
<script src="lib/jq-module/jquery.particleground.min.js" charset="utf-8"></script>
<script src="lib/layui-v2.6.3/layui.js" charset="utf-8"></script>
<script src="js/lay-config.js?v=1.0.4" charset="utf-8"></script>
<script>
    layui.use(['form','miniTab','slider'], function () {
        var form = layui.form,
            layer = layui.layer,
            miniTab = layui.miniTab;
        var slider = layui.slider;
        //渲染
        slider.render({
            elem: '#slideTest1',
            max: 10240
            ,change: function(value){
                console.log(value) //动态获取滑块数值
                $("#applySize").attr("value",value);
            }
        });

        //监听提交
        form.on('submit(saveBtn)', function (data) {
            $.ajax({
                url:'/sys/user/doApplyDiskSize',
                type:'post',
                dataType:'text',
                contentType: 'application/x-www-form-urlencoded',
                data:{
                    applySize : $("#applySize").val(),
                },
                timeout:2000,
                xhrFields: {
                    withCredentials: true //session id 保持不变
                },
                success:function(data){
                    console.log(data);
                    if(data === 'success'){
                        var index = layer.msg("申请成功");
                        layer.close(index);
                        miniTab.deleteCurrentByIframe();
                    }else{
                        layer.msg(data)
                    }
                },
                error:function () {
                    layer.msg("系统错误")
                }
            })
            return false;
        });

    });
</script>
</body>
</html>