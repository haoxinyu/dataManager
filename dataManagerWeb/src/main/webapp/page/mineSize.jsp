<%@ page import="com.dataManager.www.entity.User" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ include file="/static/taglib.jsp"%>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>layui</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="lib/layui-v2.6.3/css/layui.css" media="all">
    <link rel="stylesheet" href="css/public.css" media="all">
    <style>
      .site-demo-button{margin: 10px 0 10px 0}
    </style>
</head>
<body>
<div class="layuimini-container">
    <div class="layuimini-main">
        <fieldset class="layui-elem-field site-demo-button" style="margin-top: 30px;">
            <legend>通过挖矿获得额外的空间</legend>
            <div>
                <button type="button" class="layui-btn layui-btn-fluid" onclick="mine()">挖一次试试？</button>
            </div>
        </fieldset>
        <fieldset class="layui-elem-field site-demo-button" style="margin-top: 30px;">
            <legend>计算当前已获得的额外空间</legend>
            <div>
                <button type="button" class="layui-btn layui-btn-fluid" onclick="calculate()">获取容量</button>
            </div>
            <div>
                <legend >已获得的额外空间:</legend>
                <legend id="userMineSize"></legend>
            </div>
        </fieldset>
    </div>
</div>

<script src="lib/jquery-3.4.1/jquery-3.4.1.min.js" charset="utf-8"></script>
<script src="lib/layui-v2.6.3/layui.js" charset="utf-8"></script>
<script>
    layui.use(['form', 'table'], function () {
        var $ = layui.jquery,
            form = layui.form
    });

    function mine() {
        var img = '<img src="/images/loginbg.png">';
        var open_index = layer.open({
            type: 1,//Page层类型
            area: ['150px', '150px'],
            title: '正在挖矿',
            anim: 1 ,//0-6的动画形式，-1不开启
            content: img
        });

        $.ajax({
            url:'/sys/user/mineSize',
            type:'post',
            dataType:'text',
            contentType: 'application/x-www-form-urlencoded',
            timeout:30000,
            xhrFields: {
                withCredentials: true //session id 保持不变
            },
            success:function(data){
                console.log(data);
                if(data === 'success'){
                    var index = layer.msg("挖矿成功");
                    layer.close(open_index);
                }else{
                    layer.msg(data)
                }
            },
            error:function () {
                layer.msg("系统错误")
            }
        })
    }

    function calculate() {
        $.ajax({
            url:'/sys/user/calculateSize',
            type:'post',
            dataType:'text',
            contentType: 'application/x-www-form-urlencoded',
            timeout:30000,
            xhrFields: {
                withCredentials: true //session id 保持不变
            },
            success:function(data){
                console.log(data);
                $("#userMineSize").html(data+" MB");
            },
            error:function () {
                layer.msg("系统错误")
            }
        })
    }
</script>
</body>
</html>