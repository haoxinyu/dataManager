<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ include file="/static/taglib.jsp"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>基本资料</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="lib/layui-v2.6.3/css/layui.css" media="all">
    <link rel="stylesheet" href="css/public.css" media="all">
    <style>
        .layui-form-item .layui-input-company {width: auto;padding-right: 10px;line-height: 38px;}
    </style>
</head>
<body>
<div class="layuimini-container">
    <div class="layuimini-main">

        <div class="layui-form layuimini-form">
            <input type="hidden" id = "password" name="password" value="${user.password}">
            <input type="hidden" id ="id" name="id" value="${user.id}">
            <input type="hidden" id ="role" name="role" value="${user.role}">
            <input type="hidden" id ="status" name="status" value="${user.status}">
            <input type="hidden" id ="diskSize" name="diskSize" value="${user.diskSize}">
            <input type="hidden" id ="applySize" name="applySize" value="${user.applySize}">

            <div class="layui-form-item">
                <label class="layui-form-label required">用户名</label>
                <div class="layui-input-block">
                    <input type="text" id="username" name="username" lay-verify="required" lay-reqtext="用户名不能为空" placeholder="请输入用户名"  value="${user.username}" class="layui-input">
                    <tip>填写自己管理账号的名称。</tip>
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label required">手机</label>
                <div class="layui-input-block">
                    <input type="number" id="phone" name="phone" lay-verify="required" lay-reqtext="手机不能为空" placeholder="请输入手机"  value="${user.phone}" class="layui-input">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">邮箱</label>
                <div class="layui-input-block">
                    <input type="email" id="email" name="email" placeholder="请输入邮箱" value="${user.email}" class="layui-input">
                </div>
            </div>


            <div class="layui-form-item">
                <div class="layui-input-block">
                    <button class="layui-btn layui-btn-normal" lay-submit lay-filter="saveBtn">确认保存</button>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="lib/jquery-3.4.1/jquery-3.4.1.min.js" charset="utf-8"></script>
<script src="lib/jq-module/jquery.particleground.min.js" charset="utf-8"></script>
<script src="lib/layui-v2.6.3/layui.js" charset="utf-8"></script>
<script src="js/lay-config.js?v=1.0.4" charset="utf-8"></script>
<script>
    layui.use(['form','miniTab'], function () {
        var form = layui.form,
            layer = layui.layer,
            miniTab = layui.miniTab;
        //监听提交
        form.on('submit(saveBtn)', function (data) {
            $.ajax({
                url:'/sys/user/doUpdateAjax',
                type:'post',
                dataType:'text',
                contentType: 'application/json',
                data:JSON.stringify({
                    id : $("#id").val(),
                    username : $("#username").val(),
                    password : $("#password").val(),
                    email : $("#email").val(),
                    phone : $("#phone").val(),
                    role : $("#role").val(),
                    status : $("#status").val(),
                    diskSize : $("#diskSize").val(),
                    applySize : $("#applySize").val(),
                }),
                timeout:2000,
                xhrFields: {
                    withCredentials: true //session id 保持不变
                },
                success:function(data){
                    console.log(data);
                    if(data === 'success'){
                        var index = layer.msg("修改成功");
                        layer.close(index);
                        miniTab.deleteCurrentByIframe();
                    }else{
                        layer.msg(data)
                    }
                },
                error:function () {
                    layer.msg("系统错误")
                }
            })
            return false;
        });

    });
</script>
</body>
</html>