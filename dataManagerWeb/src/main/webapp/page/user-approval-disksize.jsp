<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ include file="/static/taglib.jsp"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>申请审批</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="lib/layui-v2.6.3/css/layui.css" media="all">
    <link rel="stylesheet" href="css/public.css" media="all">
    <style>
        .layui-form-item .layui-input-company {width: auto;padding-right: 10px;line-height: 38px;}
    </style>
</head>
<body>
<div class="layuimini-container">
    <div class="layuimini-main">

        <script type="text/html" id="toolbarDemo">
            <div class="layui-btn-container">
                <button class="layui-btn layui-btn-normal layui-btn-sm data-add-btn" lay-event="add"> 添加 </button>
                <button class="layui-btn layui-btn-sm layui-btn-danger data-delete-btn" lay-event="delete"> 删除 </button>
            </div>
        </script>

        <div class="layui-form layuimini-form">
            <table class="layui-hide" id="currentTableId" lay-filter="currentTableFilter"></table>
            <script type="text/html" id="currentTableBar">
                <a class="layui-btn layui-btn-normal layui-btn-xs data-count-edit" lay-event="allowApply">通过</a>
                <a class="layui-btn layui-btn-xs layui-btn-danger data-count-edit" lay-event="notAllowApply">不通过</a>
                <a class="layui-btn layui-btn-normal layui-btn-xs data-count-edit" lay-event="edit">编辑</a>
            </script>
        </div>
    </div>
</div>
<script src="lib/jquery-3.4.1/jquery-3.4.1.min.js" charset="utf-8"></script>
<script src="lib/jq-module/jquery.particleground.min.js" charset="utf-8"></script>
<script src="lib/layui-v2.6.3/layui.js" charset="utf-8"></script>
<script src="js/lay-config.js?v=1.0.4" charset="utf-8"></script>
<script>
    layui.use(['form', 'table'], function () {
        var $ = layui.jquery,
            form = layui.form,
            table = layui.table;
        table.render({
            elem: '#currentTableId',
            url: "/sys/user/listUsers",
            id: "userTable",
            toolbar: '#toolbarDemo',
            defaultToolbar: ['filter', 'exports', 'print', {
                title: '提示',
                layEvent: 'LAYTABLE_TIPS',
                icon: 'layui-icon-tips'
            }],
            cols: [[
                {type: "checkbox", width: 50},
                {field: 'id', width: 50, title: 'ID', sort: true},
                {field: 'username', width: 100, title: '用户名'},
                {field: 'password', width: 80, title: '密码'},
                {field: 'email', width: 80, title: '电子邮箱'},
                {field: 'phone',minWidth: 80, title: '电话' },
                {field: 'role', width: 80, title: '角色', sort: true},
                {field: 'status', width: 80, title: '状态', sort: true},
                {field: 'diskSize', width: 100, title: '硬盘空间', sort: true},
                {field: 'applySize', width: 100, title: '申请空间', sort: true},
                {title: '操作', minWidth: 150, toolbar: '#currentTableBar'}
            ]],
            limits: [10, 15, 20, 25, 50, 100],
            limit: 15,
            page: true,
            skin: 'line'
        });

        /**
         * toolbar监听事件
         */
        table.on('toolbar(currentTableFilter)', function (obj) {
            if (obj.event === 'add') {  // 监听添加操作
                var index = layer.open({
                    title: '添加用户',
                    type: 2,
                    shade: 0.2,
                    maxmin:true,
                    shadeClose: true,
                    area: ['100%', '100%'],
                    content: 'page/table/add.jsp',
                });
                $(window).on("resize", function () {
                    layer.full(index);
                });
            } else if (obj.event === 'delete') {  // 监听删除操作
                var checkStatus = table.checkStatus('currentTableId')
                    , data = checkStatus.data;
                layer.alert(JSON.stringify(data));
            }
        });

        //监听表格复选框选择
        table.on('checkbox(currentTableFilter)', function (obj) {
            console.log(obj)
        });

        table.on('tool(currentTableFilter)', function (obj) {
            var data = obj.data;
            console.info('/sys/user/editUser/'+data.username)
            if (obj.event === 'edit') {
                var index = layer.open({
                    title: '编辑用户',
                    type: 2,
                    shade: 0.2,
                    maxmin:true,
                    shadeClose: true,
                    area: ['100%', '100%'],
                    content: '/sys/user/editUser/'+data.username,
                    cancel: function(index, layero){
                        if(confirm('确定要关闭么')){
                            layer.close(index);
                            table.reload('userTable', {
                                where: {
                                    status: data.status,
                                    diskSize: data.diskSize,
                                    applySize:data.applySize,
                                    username: data.username,
                                    phone: data.phone,
                                }
                            });
                        }
                        return false;
                    }
                });
                $(window).on("resize", function () {
                    layer.full(index);
                });
                return false;
            } else if (obj.event === 'allowApply') {
                $.ajax({
                    url:'/sys/user/doAllowApplyDiskSize',
                    type:'post',
                    dataType:'text',
                    contentType: 'application/json',
                    data:JSON.stringify({
                        id : data.id,
                        username : data.username,
                        password : data.password,
                        email : data.email,
                        phone : data.phone,
                        role : data.role,
                        status : data.status,
                        diskSize : data.diskSize,
                        applySize : data.applySize,
                    }),
                    timeout:2000,
                    xhrFields: {
                        withCredentials: true //session id 保持不变
                    },
                    success:function(data){
                        layer.msg("成功")
                        table.reload('userTable', {
                            where: {
                                status: data.status,
                                diskSize: data.diskSize,
                                applySize:data.applySize
                            }
                            // , page: {
                            //     curr: 1 //重新从第 1 页开始
                            // }
                        });
                    },
                    error:function () {
                        layer.msg("系统错误")
                    }
                })

            }else if(obj.event === 'notAllowApply'){
                console.info(data)
            }
        });

    });
</script>
</body>
</html>