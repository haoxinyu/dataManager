<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ include file="/static/taglib.jsp"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>上传数据</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="lib/layui-v2.6.3/css/layui.css" media="all">
    <link rel="stylesheet" href="css/public.css" media="all">
    <link rel="stylesheet" href="js/lay-module/step-lay/step.css" media="all">
    <script src="lib/pluploadMaster/js/plupload.full.min.js"></script>
</head>
<body>
<div class="layuimini-container">
    <div class="layuimini-main">
        <div class="layui-fluid">
            <div class="layui-card">
                <div class="layui-card-body" style="padding-top: 40px;">
                    <div class="layui-carousel" id="stepForm" lay-filter="stepForm" style="margin: 0 auto;">
                        <div carousel-item>
                            <div style="overflow-y: auto;">
                                <form class="layui-form" style="margin: 0 auto;max-width: 600px;padding-top: 40px;">
                                    <div class="layui-form-item">
                                        <div class="layui-upload">
                                            <button type="button" class="layui-btn layui-btn-normal" id="testList">添加</button>
                                            <div class="layui-upload-list">
                                                <table class="layui-table">
                                                    <thead>
                                                    <tr>
                                                        <th>文件名</th>
                                                        <th>上传进度</th>
                                                        <th>状态</th>
                                                        <th>操作</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody id="demoList"></tbody>
                                                </table>
                                            </div>
                                            <button type="button" class="layui-btn" id="uploadfiles">开始上传</button>
                                        </div>
                                    </div>
                                    <div class="layui-form-item">
                                        <div class="layui-input-block">
                                            <button class="layui-btn" lay-submit lay-filter="formStep" id="uploadData">
                                                &emsp;下一步&emsp;
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>

                            <div>
                                <form class="layui-form" style="margin: 0 auto;max-width: 460px;padding-top: 40px;">
                                    <div class="layui-form-item">
                                        <label class="layui-form-label">文件（文件组）名称</label>
                                        <div class="layui-input-block">
                                            <input type="text" placeholder="请填写文件名称" class="layui-input" lay-verify="required" required
                                                   name="fileName" id="fileName" />

                                        </div>
                                    </div>
                                    <div class="layui-form-item">
                                        <label class="layui-form-label">文件标签</label>
                                        <div class="layui-btn-container tag" lay-filter="demo" lay-allowclose="true" lay-newTag="true">
                                        </div>
                                        <div class="layui-input-block">
                                            <input type="hidden" value="" class="layui-input" readonly
                                                   name="labels" id="labels">
                                        </div>
                                    </div>

                                    <div class="layui-form-item">
                                        <div class="layui-input-block">
                                            <button class="layui-btn" lay-submit lay-filter="formStep2">
                                                &emsp;下一步&emsp;
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<script src="lib/layui-v2.6.3/layui.js" charset="utf-8"></script>
<script src="js/lay-config.js?v=1.0.4" charset="utf-8"></script>
<script src="lib/jquery-3.4.1/jquery-3.4.1.min.js" charset="utf-8"></script>
<script>
    layui.use([ 'form', 'step'], function () {
        var $ = layui.$,
            form = layui.form,
            step = layui.step;
        step.render({
            elem: '#stepForm',
            filter: 'stepForm',
            width: '100%', //设置容器宽度
            stepWidth: '750px',
            height: '500px',
            stepItems: [{
                title: '上传数据文件'
            }, {
                title: '填写数据信息'
            }, {
                title: '指定数据位置和公开方式'
            }]
        });


        form.on('submit(formStep)', function (data) {
            step.next('#stepForm');
            return false;
        });

        form.on('submit(formStep2)', function (data) {
            var list = $(".tag-item")
            $("#labels").val(list.text());

            $.ajax({
                url:'/sys/file/addUploadInfo',
                type:'post',
                dataType:'text',
                contentType: 'application/json',
                data:JSON.stringify({
                    fileName : $("#fileName").val(),
                    labels : $("#labels").val()
                }),
                timeout:2000,
                xhrFields: {
                    withCredentials: true //session id 保持不变
                },
                success:function(data){
                    console.log(data);
                    if(data === 'success'){
                        var index = layer.msg("修改成功");
                        step.next('#stepForm');
                    }else{
                        layer.msg(data)
                    }
                },
                error:function () {
                    layer.msg("系统错误")
                }
            });
            return false;
        });

        $('.pre').click(function () {
            step.pre('#stepForm');
        });

        $('.next').click(function () {
            step.next('#stepForm');
        });
        $('#uploadData').addClass("layui-btn-disabled").attr("disabled",true);
    })
</script>

<script>
    var status = 0;
    layui.use(['upload','element','layer'], function(){
        var $ = layui.jquery;
        var upload = layui.upload,element = layui.element,layer = layui.layer;
        //实例化一个plupload上传对象
        var uploader = new plupload.Uploader({
            browse_button : 'testList', //触发文件选择对话框的按钮，为那个元素id
            url : '/sys/file/plupload', //服务器端的上传页面地址
            flash_swf_url : 'lib/pluploadMaster/js/Moxie.swf', //swf文件，当需要使用swf方式进行上传时需要配置该参数
            silverlight_xap_url : 'lib/pluploadMaster/js/Moxie.xap' ,//silverlight文件，当需要使用silverlight方式进行上传时需要配置该参数
            max_file_size : '10240mb',
            chunk_size : '10mb',
            resize : {
                width : 200,
                height : 200,
                quality : 90,
                crop : true
            },
            init : {
                PostInit : function() {
                    $("#uploadfiles").click (function() {
                        var isUpload = confirm("是否确定要上传文件?");
                        if(isUpload){
                            uploader.start();// 开始上传
                            return false;
                        }
                    });
                },
                FilesAdded : function(up, files) {
                    plupload.each(files, function(file) {
                        console.log(file,"file");
                        var tr = $(['<tr id="'+ file.id +'" class="uptr">'
                            ,'<td>'+ file.name +'</td>'
                            ,'<td>'
                            +'<div  file="'+file.name+'" class="layui-progress layui-progress-big"  lay-showPercent="true"   lay-filter="'+file.id+'">'
                            +'<div  class="layui-progress-bar layui-bg-red" lay-percent="0%"><span class="layui-progress-text">0%</span></div>'
                            /*  +'<input name="pr" value="'+file.name+'" type="hidden" >'*/
                            +'</div>'
                            , '</td>'
                            ,'<td class="upfile">等待上传</td>'
                            ,'<td>'
                            ,'<button class="layui-btn layui-btn-xs demo-reload layui-hide">重传</button>'
                            ,'<button class="layui-btn layui-btn-xs layui-btn-danger demo-stop">暂停</button>'
                            ,'<button class="layui-btn layui-btn-xs layui-btn-danger demo-start">继续</button>'
                            ,'<button class="layui-btn layui-btn-xs layui-btn-danger demo-delete">删除</button>'
                            ,'</td>'
                            ,'</tr>'].join(''));
                        $('#demoList').append(tr);

                    });
                    plupload.each(files, function(file) {  //主要针对每个文件的删除操作
                        $("#"+file.id+" .demo-delete").click(function() {
                            console.log(file,"file删除触发")
                            if(file.status==2){
                                alert("文件《"+file.name+"》正在上传，请不要删除！");
                            }else if(file.status==5){
                                alert("文件《"+file.name+"》已经上传成功，不可以删除！");
                            }else{
                                var gnl = confirm("确定要删除《"+file.name+"》?");
                                if (gnl == true) {
                                    $("#"+file.id).remove();
                                    uploader.removeFile(file);
                                } else {
                                    return false;
                                }
                            }
                        });
                        $("#"+file.id+" .demo-stop").click(function() {
                            uploader.stop();// 停止上传
                            console.log(file,"file暂停")
                        });
                        $("#"+file.id+" .demo-start").click(function() {
                            uploader.start();// 继续上传
                            console.log(file,"file开始")
                        });
                    });
                },
                UploadProgress : function(up, file) {//显示文件上传的状态
                    //_mask();
                    var percent = file.percent;
                    element.progress(file.id, percent+"%")
                    //_mask();
                },// 文件上传成功的时候触发，针对于每一个文件；这里主要返回文件上传成功与否的状态，
                FileUploaded : function(up, file, info) {
                    console.info(info.response)
                     var data = info.response;// 解析返回的json数据
                    if (data == "freeDiskSizeEmpty") {
                        status = 1;
                    }
                },
                UploadComplete : function(up, files) {//队列中的所有文件上传完后，触发
                    if(files.length<=0){
                        alert("请先添加文件进行，并上传！");
                    }else{
                        var arr2 = new Array();
                        plupload.each(files, function(file) {
                            console.log(file.status);
                            if (file.status == 5) {//将上传成功的文件信息发送到后台进行处理
                                var json = {
                                    docId:file.id,
                                    docName : file.name,
                                    fileSize : plupload.formatSize(file.size)
                                };
                                arr2.push(json);
                            }
                        });
                        console.log(arr2);
                        arr2 = JSON.stringify(arr2);
                        $("#uploadfilelist").val(arr2);//将结果传给前台，以便统一操作
                        if(status==1){
                            alert("用户空间已满！");
                        }else{
                            $('#uploadData').removeClass("layui-btn-disabled").attr("disabled",false);
                        }
                    }
                },
                Error : function(up, err,file) { // 上传出错的时候触发
                    if(err.code=="-600"){
                        alert("文件："+err.file.name+"太大，超过100mb!");
                    }else{
                        alert( err.file.name+"添加上传队列失败！错误原因："+err.message);
                    }
                }
            }
        });

        //在实例对象上调用init()方法进行初始化
        uploader.init();

        //绑定各种事件，并在事件监听函数中做你想做的事
        uploader.bind('FilesAdded',function(uploader,files){
            //每个事件监听函数都会传入一些很有用的参数，
            //我们可以利用这些参数提供的信息来做比如更新UI，提示上传进度等操作
        });
        uploader.bind('UploadProgress',function(uploader,file){
            //每个事件监听函数都会传入一些很有用的参数，
            //我们可以利用这些参数提供的信息来做比如更新UI，提示上传进度等操作
        });
        //......
        //......

        /*    //最后给"开始上传"按钮注册事件
            document.getElementById('start_upload').onclick = function(){
                uploader.start(); //调用实例对象的start()方法开始上传文件，当然你也可以在其他地方调用该方法
            } */
    });
</script>

<script>
    //config的设置是全局的
    layui.config({
        base: 'lib/layui-v2.6.3/modules/' //假设这是你存放拓展模块的根目录
    }).extend({ //设定模块别名
        tag: 'tag' //如果 tag.js 是在根目录，也可以不用设定别名
    }).use('tag', function () {
        var $ = layui.jquery
            , tag = layui.tag; //Tag的切换功能，切换事件监听等，需要依赖tag模块

        tag.on('add(demo)', function (data) {
            console.log('新增');
            console.log(data);
            console.info(data.elem.text())
        });

        tag.on('delete(demo)', function (data) {
            console.log('删除');
            console.log(data);
        });
    });
</script>



</body>
</html>