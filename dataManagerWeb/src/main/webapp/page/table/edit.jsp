<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ include file="/static/taglib.jsp"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>layui</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="lib/layui-v2.6.3/css/layui.css" media="all">
    <link rel="stylesheet" href="css/public.css" media="all">
    <style>
        body {
            background-color: #ffffff;
        }
    </style>
</head>
<body>
<div class="layui-form layuimini-form">
    <input type="hidden" id = "password" name="password" value="${editUser.password}">
    <input type="hidden" id ="id" name="id" value="${editUser.id}">
    <input type="hidden" id ="diskSize" name="diskSize" value="${editUser.diskSize}">
    <input type="hidden" id ="applySize" name="applySize" value="${editUser.applySize}">

    <div class="layui-form-item">
        <label class="layui-form-label required">用户名</label>
        <div class="layui-input-block">
            <input type="text" name="username" id="username" lay-verify="required" lay-reqtext="用户名不能为空" placeholder="请输入用户名" value="${editUser.username }" class="layui-input">
            <tip>填写自己管理账号的名称。</tip>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label required">手机</label>
        <div class="layui-input-block">
            <input type="number" name="phone" id="phone" lay-verify="required" lay-reqtext="手机不能为空" placeholder="请输入手机" value="${editUser.phone }" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">邮箱</label>
        <div class="layui-input-block">
            <input type="email" name="email" id="email" placeholder="请输入邮箱" value="${editUser.email }" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">角色</label>
        <div class="layui-input-block">
            <input type="text" name="role" id="role" placeholder="请输入角色" value="${editUser.role }" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label required">状态</label>
        <div class="layui-input-block">
            <c:choose>
                <c:when test="${editUser.status} eq '封禁'">
                    <input type="radio" name="status" value="正常" title="正常" >
                    <input type="radio" name="status" value="封禁" title="封禁" checked="">
                </c:when>
                <c:otherwise>
                    <input type="radio" name="status" value="正常" title="正常" checked="">
                    <input type="radio" name="status" value="封禁" title="封禁">
                </c:otherwise>
            </c:choose>
        </div>
    </div>

    <div class="layui-form-item">
        <div class="layui-input-block">
            <button class="layui-btn layui-btn-normal" lay-submit lay-filter="saveBtn">确认保存</button>
        </div>
    </div>
</div>
</div>
<script src="lib/layui-v2.6.3/layui.js" charset="utf-8"></script>
<script>
    function check(){
        var radio = document.getElementsByName("status");
        for(i = 0; i < radio.length; i ++){
            if(radio[i].checked){
                return radio[i].value;
            }
        }
    }

    layui.use(['form'], function () {
        var form = layui.form,
            layer = layui.layer,
            $ = layui.$;
        //监听提交
        form.on('submit(saveBtn)', function (data) {
            $.ajax({
                url:'/sys/user/doUpdateAjax',
                type:'post',
                dataType:'text',
                contentType: 'application/json',
                data:JSON.stringify({
                    id : $("#id").val(),
                    username : $("#username").val(),
                    password : $("#password").val(),
                    email : $("#email").val(),
                    phone : $("#phone").val(),
                    role : $("#role").val(),
                    status : check(),
                    diskSize : $("#diskSize").val(),
                    applySize : $("#applySize").val(),
                }),
                timeout:2000,
                xhrFields: {
                    withCredentials: true //session id 保持不变
                },
                success:function(data){
                    console.log(data);
                    if(data === 'success'){
                        var index = layer.msg("修改成功");
                    }else{
                        layer.msg(data)
                    }
                },
                error:function () {
                    layer.msg("系统错误")
                }
            })
            return false;
        });

    });
</script>
</body>
</html>
