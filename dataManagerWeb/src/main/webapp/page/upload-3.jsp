<%@ page language="java" contentType="text/html; charset=UTF-8"
		 pageEncoding="UTF-8"%>
<%@ include file="/static/taglib.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title>My JSP 'plupload.jsp' starting page</title>
	<link rel="stylesheet" href="lib/layui-v2.6.3/css/layui.css" media="all">
	<link rel="stylesheet" href="css/public.css" media="all">
	<!-- 首先需要引入plupload的源代码 -->
<script src="lib/pluploadMaster/js/plupload.full.min.js"></script>
</head>

<body>
<div class="layuimini-container">
	<div class="layuimini-main">
		<form action="/sys/file/upload" method="post" enctype="multipart/form-data">
			<input type="file" name="file" size="50" />
			<br />
			<button type="submit">上传</button>
		</form>
	</div>
</div>

</body>
</html>
