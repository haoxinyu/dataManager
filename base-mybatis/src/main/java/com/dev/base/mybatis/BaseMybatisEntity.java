package com.dev.base.mybatis;

import java.io.Serializable;
import java.util.Date;


public class BaseMybatisEntity implements Serializable{
		
	private static final long serialVersionUID = 1L;

	public Long id;
	
	public Date createDate = new Date();
	
	public Date modifyDate = new Date();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}
}
