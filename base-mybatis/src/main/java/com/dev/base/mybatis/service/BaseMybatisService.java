package com.dev.base.mybatis.service;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.dev.base.mybatis.BaseMybatisEntity;


public interface BaseMybatisService<T extends BaseMybatisEntity, ID extends Serializable> {
	/**
	 * 
			*@name 新增对象
			*@Description  
			*@CreateDate
	 */
	T add(T param);

	/**
	 * 
			*@name 批量新增
			*@Description  
			*@CreateDate
	 */
	void batchAdd(List<T> batchList);
	
	/**
	 * 
			*@name 修改对象
			*@Description  
			*@CreateDate
	 */
	T update(T param);

	/**
	 * 
			*@name 删除对象
			*@Description  
			*@CreateDate
	 */
	void delete(T param);

	/**
	 * 
			*@name 根据id进行删除
			*@Description  
			*@CreateDate
	 */
	void deleteById(ID id);
	
	/**
	 * 
			*@name 分页查询列表
			*@Description  
			*@CreateDate
	 */
	List<T> listPage(int pageNumber, int pageSize, Map<String, Object> paramMap);

	/**
	 * 
			*@name 查询数目
			*@Description  
			*@CreateDate
	 */
	long count(Map paramMap);

	/**
	 * 
			*@name 获取指定对象记录
			*@Description  
			*@CreateDate
	 */
	T get(Map paramMap);
	
	/**
	 * 
			*@name 根据id获取对象
			*@Description  
			*@CreateDate
	 */
	T getById(ID id);

	List<T> listByObject(T param);
}
