package com.dev.base.mybatis.utils.transaction;


public interface ITransaction {
	/**
	 * 
			*@name 业务执行
			*@Description  将需要事务处理的逻辑在exec中实现,如果有异常则自动回滚
			*@CreateDate
	 */
	void exec() throws Exception;
}
