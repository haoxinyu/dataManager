package com.dev.base.jdbc.datasource;

import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

/**
 * 自定义多数据源类
 */
public class MultipleDataSource extends AbstractRoutingDataSource{
	
	@Override
	protected Object determineCurrentLookupKey() {
//		System.out.println("当前数据源："+DataSourceContextHolder.getCurrent());
		return DataSourceContextHolder.getCurrent();
	}
}
