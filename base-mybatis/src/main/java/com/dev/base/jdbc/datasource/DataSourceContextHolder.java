package com.dev.base.jdbc.datasource;

/**
 * 保存当前数据源
 */
public class DataSourceContextHolder {
    private static final ThreadLocal<String> contextHolder = new ThreadLocal<String>();  
    
    /**
     * 
    		*@name 指定当前使用的数据源
    		*@Description  
    		*@CreateDate
     */
    public static void setCurrent(String dataSource) {  
        contextHolder.set(dataSource);  
    }  
      
    /**
     * 
    		*@name 获取当前数据源
    		*@Description  
    		*@CreateDate
     */
    public static String getCurrent() {  
        return contextHolder.get();  
    }  
      
    /**
     * 
    		*@name 清除数据源
    		*@Description  
    		*@CreateDate
     */
    public static void clear() {  
        contextHolder.remove();  
    }  
}
