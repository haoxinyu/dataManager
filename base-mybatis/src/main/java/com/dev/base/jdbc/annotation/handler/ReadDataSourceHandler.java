package com.dev.base.jdbc.annotation.handler;

import org.aspectj.lang.ProceedingJoinPoint;
import org.springframework.util.StringUtils;

import com.dev.base.jdbc.annotation.ReadDataSource;
import com.dev.base.jdbc.datasource.DataSourceContextHolder;

/**
 * 处理只读注解
 */
public class ReadDataSourceHandler {
	/** 默认只读数据源key*/
	private String defaultTargetDataSource;
	
	/**
	 * 
			*@name 设置数据源
			*@Description  
			*@CreateDate
	 */
	public Object doAround(ProceedingJoinPoint joinPoint,ReadDataSource ds) throws Throwable {
		String dataSource = StringUtils.isEmpty(ds.value()) ? defaultTargetDataSource : ds.value(); 
        DataSourceContextHolder.setCurrent(dataSource);
        try{
            return joinPoint.proceed();
        }
        catch(Exception e){
        	e.printStackTrace();
            return null;
        }
        finally{
        	DataSourceContextHolder.clear();
        }
    }
	
	public String getDefaultTargetDataSource() {
		return defaultTargetDataSource;
	}

	public void setDefaultTargetDataSource(String defaultTargetDataSource) {
		this.defaultTargetDataSource = defaultTargetDataSource;
	} 
}
